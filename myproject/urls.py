"""myproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from myapp import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),

    path('',views.firstpage,name="homepage"),

    # path('message',views.message),
    path('contacts',views.contacts,name="cont"),

    path('student/',views.student,name="stuprofile"),

    path('user_login',views.user_login,name="user_login"),

    path('register',views.register,name="register"),

    path('editpro',views.profile,name="editpro"),

    path('reset',views.reset,name="reset"),

    path('about',views.about,name="about"),

    path('allcourses',views.allcourse,name="allcourses"),

    path('insprofile',views.insp,name="insprofile"),

    path('inscourses',views.insc,name="inscourses"),
    
    path('check_user',views.check_user,name="check_user"),

    path('view_profile',views.view_profile,name="view_profile"),
    
    path('ins_view',views.ins_view,name="ins_view"),

    path('add_course',views.add_course_view,name="add_course_view"),
    
    path('wishlist',views.wishlist,name="wishlist"),

    path('my_courses/',views.my_courses,name="my_courses"),

    path('singlecourse',views.single,name="singlecourse"),

    path('update_course',views.update_course,name="update_course"),

    path('delete_course',views.delete_course,name="delete_course"),

    path('sendemail',views.sendemail,name="sendemail"),
    
    path('forgotpass',views.forgotpass,name="forgotpass"),

    path('reset_password',views.reset_password,name="reset_password"),

    path('cart',views.add_to_cart,name="cart"),

    path('get_cart_data',views.get_cart_data,name="get_cart_data"),

    path('change_quan',views.change_quan,name="change_quan"),

    path('paypal/', include('paypal.standard.ipn.urls')),

    path('process_payment',views.process_payment,name="process_payment"),

    path('payment_done',views.payment_done,name="payment_done"),

    path('payment_cancelled',views.payment_cancelled,name="payment_cancelled"),

    path('order_history',views.order_history,name="order_history"),

    path('my_student',views.my_student,name="my_student"),

    path('user_logout',views.user_logout,name="user_logout")
    # path('course',views.single),
    # path('review',views.review),
    # path('instructor',views.ins),
]+static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
