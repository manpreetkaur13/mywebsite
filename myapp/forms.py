from django import forms
from myapp.models import Add_Course

class Add_Course_form(forms.ModelForm):
    class Meta:
        model = Add_Course
        # fields = "__all__"
        exclude = ["instructor"]
        # fields = ["course_name","course_category","course_price","sale_price","course_image","course_details"]