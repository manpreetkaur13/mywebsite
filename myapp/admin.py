from django.contrib import admin
from myapp.models import Student,Contact,Category,register_table,Add_Course,Cart,Order

admin.site.site_header = "Pioneer Python"
# Register your models here.
class StudentAdmin(admin.ModelAdmin):
    # fields = ["email","name","roll_no"] 
    list_display = ["name","roll_no","email","fee","gender","address","is_registered"]
    search_fields =["name","roll_no"]
    list_filter =["name","gender"]
    list_editable =["email"]


class ContactAdmin(admin.ModelAdmin):
    # fields = ["email","subject","message"]
    list_display = ["id","name","email","subject","message","added_on"]
    search_fields = ["name","email"]
    list_filter = ["name","email"]
    list_editable = ["email"]


class CategoryAdmin(admin.ModelAdmin):
    list_display = ["id","cat_name","description","added_on"]

admin.site.register(Student,StudentAdmin)
admin.site.register(Contact,ContactAdmin)
admin.site.register(Category,CategoryAdmin)
admin.site.register(register_table)
admin.site.register(Add_Course)
admin.site.register(Cart)
admin.site.register(Order)

