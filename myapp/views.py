from django.shortcuts import render, get_object_or_404,reverse
from django.http import HttpResponse,HttpResponseRedirect,JsonResponse
from myapp.models import Contact,Category,register_table,Add_Course,Cart,Order
from django.contrib.auth.models import User
from django.contrib.auth import login,authenticate,logout
from django.contrib.auth.decorators import login_required
from myapp.forms import Add_Course_form
from django.db.models import Q
from datetime import datetime
from django.core.mail import EmailMessage


def firstpage(abc):
    if "user_id" in abc.COOKIES:
        uid = abc.COOKIES["user_id"]
        usr = get_object_or_404(User,id=uid)
        login(abc,usr)

        if usr.is_superuser:
            return HttpResponseRedirect("/admin")
        if usr.is_active:
            return HttpResponseRedirect("/insprofile")

        
    recent =Contact.objects.all().order_by("-id")[:5]
    cats = Category.objects.all().order_by("cat_name")
    return render(abc,"prohome.html",{"message":recent,"category":cats})

def about(abc):
    return render(abc,"pabout.html")
def contacts(abc):
    # print("DATA=",abc.POST)
    all_data = Contact.objects.all().order_by("-id")
    if abc.method=="POST":
        nm = abc.POST["name"]
        email = abc.POST["email"]
        sub = abc.POST["subject"]
        msz = abc.POST["message"]
        # return HttpResponse("<h1>"+nm+email+sub+msz+"</h1>")
        data = Contact(name=nm,email=email,subject=sub,message=msz)
        data.save()
        # res="Dear {} Thanks for Your Feedback".format(nm)
        # return render(abc,"contact.html",{"status":res})
        # return HttpResponse("<h1 style='color:green'>Dear {} Data Saved Successfully!!!</h1>".format(nm))
    
    return render(abc,"contact.html",{"message":all_data})

@login_required
def student(abc):
    context={}
    check = register_table.objects.filter(user__id=abc.user.id)
    if len(check)>0:
        data =register_table.objects.get(user__id=abc.user.id)
        context["data"]=data
    return render(abc,"pinsprofile.html",context)

def user_login(abc):
    if abc.method=="POST":
        un = abc.POST["username"]
        pwd = abc.POST["password"]
        # print(abc.POST)
        
        user = authenticate(username=un,password=pwd)
        # return HttpResponse(user)
        if user:
            login(abc,user)
            if user.is_superuser:
                return HttpResponseRedirect("/admin")
            else:
                res = HttpResponseRedirect("/insprofile")
                if "rememberme" in abc.POST:
                    res.set_cookie("user_id",user.id)
                    res.set_cookie("date_login",datetime.now()) 
                return res
            # if user.is_active:
            #     return HttpResponseRedirect("/student")

        else:
            return render(abc,"plogin.html",{"status":"Invalid Username or Password"})
    return render(abc,"plogin.html")

def profile(abc):
    context = {}
    check = register_table.objects.filter(user__id=abc.user.id)
    print(check,len(check))
    if len(check)>0:
        data =register_table.objects.get(user__id=abc.user.id)
        context["data"]=data
    if abc.method=="POST":
        print(abc.POST)
        fn = abc.POST["fname"]
        ln = abc.POST["lname"]
        occu = abc.POST["occ"]
        age = abc.POST["age"]
        con = abc.POST["contact"]
        em = abc.POST["email"]
        ct = abc.POST["city"]
        gen = abc.POST["gender"]
        abt= abc.POST["about"]

        usr = User.objects.get(id=abc.user.id)
        usr.first_name = fn
        usr.last_name = ln
        usr.email = em
        usr.save()

        data.contact_number = con
        data.age = age
        data.occupation = occu
        data.city = ct
        data.age = age
        data.gender = gen
        data.about = abt
        data.save()

        if "image" in abc.FILES:
            img = abc.FILES["image"]
            data.profile_pic = img
            data.save()


        context["status"]= "Changes Saved Successfully"
    return render(abc,"peditprofile.html",context)

def reset(abc):
    context={}
    ch = register_table.objects.filter(user__id=abc.user.id)
    if len(ch)>0:
        data =register_table.objects.get(user__id=abc.user.id)
        context["data"]=data
    if abc.method=="POST":
        current = abc.POST["cpwd"]
        new_pas = abc.POST["npwd"]
        # print(current,new_pas)
        user = User.objects.get(id=abc.user.id)
        un = user.username
        # pwd = new_pas
        check = user.check_password(current)
        # print(check)
        if check==True:
            user.set_password(new_pas)
            user.save()
        
            context["msz"] = "Password Changed Successfully!!!"
            context["col"] = "alert-success"
            user = User.objects.get(username=un)
            login(abc,user)
        else:
            context["msz"] = "Incorrect Current Password"
            context["col"] = "alert-danger"

    # data =register_table.objects.get(user__id=abc.user.id)
    # context["data"]=data
    return render(abc,"preset.html",context)


def register(abc):
    # if "user_id" in abc.COOKIES:
    #     uid = abc.COOKIES["user_id"]
    #     usr = get_object_or_404(User,id=uid)
    #     login(abc,usr)
    #     if usr.is_superuser:
    #         return HttpResponseRedirect("/admin")
    #     if usr.is_active:
    #         return HttpResponseRedirect("/insprofile")
    if abc.method=="POST":
        fname = abc.POST["first"]
        last = abc.POST["last"]
        un = abc.POST["uname"]
        em = abc.POST["email"]
        con = abc.POST["contact"]
        pwd = abc.POST["password1"]
        gn = abc.POST["gender"]
        tp = abc.POST["utype"]
        # print(abc.POST)

        usr = User.objects.create_user(un,em,pwd)
        usr.first_name = fname
        usr.last_name = last
        if tp=="inst":
            usr.is_staff = True
        usr.save()
        
        reg = register_table(user=usr,contact_number=con,gender=gn)
        reg.save()
        # return HttpResponse("Register Successfully!!!")
        # return render(abc,"pregister.html")
        # {"status":"{} Your Account Created Succesfully!!!".format(fname)}
    return render(abc,"pregister.html")
def check_user(abc):
    if abc.method=="GET":
        un = abc.GET["usern"]
        # print(un)
        check = User.objects.filter(username=un)
        # print(check,len(check))
        # return HttpResponse(check)
        if len(check) == 1:
            return HttpResponse("Exists")
        else:
            return HttpResponse("Not Exists")

@login_required
def insp(abc):
    context={}
    check = register_table.objects.filter(user__id=abc.user.id)
    if len(check)>0:
        data =register_table.objects.get(user__id=abc.user.id)
        context["data"]=data
    return render(abc,"pinsprofile.html",context)

def insc(abc):
    context={}
    check = register_table.objects.filter(user__id=abc.user.id)
    if len(check)>0:
        data =register_table.objects.get(user__id=abc.user.id)
        context["data"]=data

    return render(abc,"pinscourses.html",context)
    
@login_required
def user_logout(abc):
    logout(abc)
    res = HttpResponseRedirect("/")
    res.delete_cookie("user_id")
    res.delete_cookie("date_login")
    return res

def view_profile(abc):
    data =register_table.objects.get(user__id=abc.user.id)
    return render(abc,"sproview.html",{"data":data})

def ins_view(abc):
    data =register_table.objects.get(user__id=abc.user.id)
    return render(abc,"insproview.html",{"data":data})


def wishlist(abc):
    data =register_table.objects.get(user__id=abc.user.id)
    return render(abc,"wishlist.html",{"data":data})


def add_course_view(abc):
    context={}
    ch = register_table.objects.filter(user__id=abc.user.id)
    if len(ch)>0:
        data =register_table.objects.get(user__id=abc.user.id)
        context["data"]=data
    form = Add_Course_form()
    if abc.method=="POST":
        form = Add_Course_form(abc.POST,abc.FILES)
        if form.is_valid():
            data = form.save(commit=False)
            login_user = User.objects.get(username=abc.user.username)
            data.instructor = login_user
            data.save()
            context["status"] ="{} Added Successfully".format(data.course_name)
    context["form"] = form

    return render(abc,"addcourse.html",context)

def my_courses(abc):
    context = {}
    ch = register_table.objects.filter(user__id=abc.user.id)
    if len(ch)>0:
        data =register_table.objects.get(user__id=abc.user.id)
        context["data"]=data
    all = Add_Course.objects.filter(instructor__id=abc.user.id).order_by("-id")
    context["courses"] = all
    return render(abc,"mycourse.html",context)

def single(abc):
    context = {}
    ch = register_table.objects.filter(user__id=abc.user.id)
    if len(ch)>0:
        data =register_table.objects.get(user__id=abc.user.id)
        context["data"]=data
    id = abc.GET["cid"]
    obj =Add_Course.objects.get(id=id)
    context["course"] = obj
    return render(abc,"psinglecourse.html",context)

def update_course(abc):
    context = {}
    ch = register_table.objects.filter(user__id=abc.user.id)
    if len(ch)>0:
        data =register_table.objects.get(user__id=abc.user.id)
        context["data"]=data
    cats = Category.objects.all().order_by("cat_name")
    context["category"] = cats

    cid = abc.GET["cid"]
    # course = Add_Course.objects.get(id=cid) 
    course = get_object_or_404(Add_Course,id=cid) 
    context["course"] = course

    if abc.method=="POST":
        cn = abc.POST["cname"]
        ct_id = abc.POST["ccat"]
        pr = abc.POST["cp"]
        sr = abc.POST["sp"]
        des = abc.POST["des"]

        cat_obj = Category.objects.get(id=ct_id)

        course.course_name = cn
        course.course_category = cat_obj
        course.course_price = pr
        course.sale_price = sr
        course.course_details = des
        if "cimg" in abc.FILES:
            img = abc.FILES["cimg"]
            course.course_image = img
        course.save()
        context["status"] = "Changes Saved Successfully"
        context["id"] = cid
    return render(abc,"update_course.html",context,)

def delete_course(abc):
    context = {}
    #   context = {}
    ch = register_table.objects.filter(user__id=abc.user.id)
    if len(ch)>0:
        data =register_table.objects.get(user__id=abc.user.id)
        context["data"]=data
    if "cid" in abc.GET:
        cid = abc.GET["cid"]
        courses = get_object_or_404(Add_Course,id=cid)
        # cou = Add_Course.objects.get(id=cid)
        context["courses"] = courses
        
        if "action" in abc.GET:
            courses.delete()
            context["status"] = str(courses.course_name)+"Removed Successfully!!"
    return render(abc,"deletecourse.html",context)

def allcourse(abc):
    context = {}
    all_courses = Add_Course.objects.all().order_by("course_name")
    context["courses"] = all_courses
    if "qry" in abc.GET:
        q = abc.GET["qry"]
        p = abc.GET["price"]
        cours = Add_Course.objects.filter(course_name__contains=q)
        cours = Add_Course.objects.filter(Q(course_name__contains=q)& Q(sale_price__lt=p))
        # cours = Add_Course.objects.filter(course_name__icontains=q)
        context["courses"] = cours
        context["abcd"] = "search"

    return render(abc,"allcourses.html",context)

def sendemail(abc):
    context = {}
    ch = register_table.objects.filter(user__id=abc.user.id)
    if len(ch)>0:
        data =register_table.objects.get(user__id=abc.user.id)
        context["data"]=data
     
    if abc.method=="POST":
        # print(abc.POST)
        rec = abc.POST["to"].split(",")
        print(rec)
        sub = abc.POST["sub"]
        msz = abc.POST["msz"]
        try:
            em = EmailMessage(sub,msz,to=rec)
            em.send()
            context["status"] = "Email Sent"
            context["cls"] = "style='background-color:green' "
        except:
            context["status"] = "Could not Send Email Please Check your Internet Connection/Email Address"
            context["cls"] = "alert-danger"
    return render(abc,"sendemail.html",context )

def forgotpass(abc):
    context = {}
    if abc.method=="POST":
        un = abc.POST["username"]
        pwd = abc.POST["npass"]

        user = get_object_or_404(User,username=un)
        user.set_password(pwd)
        user.save()
        login(abc,user)
        if user.is_superuser:
            return HttpResponseRedirect("/admin")
        else:
            return HttpResponseRedirect("/insprofile")


        # context["status"] = "Password Change Successfully!!!"

    return render(abc,"forgot_pass.html",context)

import random

def reset_password(abc):
    # print("data==",abc.GET)
    # return HttpResponse("called")
    un = abc.GET["username"]
    try:
        user =get_object_or_404(User,username=un)
        otp = random.randint(1000,9999)
        msz = "Dear {} \n{} is your One Time Password(OTP) \nDo not share it with others \nThanks&Regards \nPioneerPython".format(user.username,otp)
        try:
            email = EmailMessage("Account Verification",msz,to=[user.email])
            email.send()
            return JsonResponse({"status":"sent","email":user.email,"rotp":otp})
        except:
            return JsonResponse({"status":"error","email":user.email})
    except:
        return JsonResponse({"status":"failed"})


def add_to_cart(abc):
    context = {}
    items = Cart.objects.filter(user__id=abc.user.id,status=False)
    context["items"] = items
    if abc.user.is_authenticated:
       if abc.method=="POST":
           cid = abc.POST["cid"]
           qty = abc.POST["qty"]
           is_exist = Cart.objects.filter(course__id=cid,user__id=abc.user.id,status=False)
           if len(is_exist)>0:
               context["msz"] = "Item Already Exists in Your Cart"
               context["cls"] = "alert alert-warning"
           else:
               course = get_object_or_404(Add_Course,id=cid)
               usr = get_object_or_404(User,id=abc.user.id)
               c = Cart(user=usr,course=course,quantity=qty)
               c.save()
               context["msz"] = "{} Added in Your Cart".format(course.course_name)
               context["cls"] = "alert alert-success"

    else:
        context["status"] = "Please Login first to View Your Cart!!!"
    return render(abc,"cart.html",context)

def get_cart_data(abc):
    items = Cart.objects.filter(user__id=abc.user.id, status=False)
    sale,total,quantity = 0,0,0
    for i in items:
        sale += float(i.course.sale_price)*i.quantity
        total += float(i.course.course_price)*i.quantity
        quantity += int(i.quantity)

    res ={
        "total":total,"offer":sale,"quan":quantity,
    }
    return JsonResponse(res)

def change_quan(abc):
    if "quantity" in abc.GET:
        cid = abc.GET["cid"]
        qty = abc.GET["quantity"]
        cart_obj = get_object_or_404(Cart,id=cid)
        cart_obj.quantity = qty
        cart_obj.save()
        return HttpResponse(cart_obj.quantity)

    if "delete_cart" in abc.GET:
        id = abc.GET["delete_cart"]
        cart_obj = get_object_or_404(Cart,id=id)
        cart_obj.delete()
        return HttpResponse(1)

from paypal.standard.forms import PayPalPaymentsForm
from django.conf import settings
def process_payment(abc):
    items = Cart.objects.filter(user_id__id=abc.user.id,status=False)
    courses =""
    amt=0
    inv = "INV-" 
    cart_ids =""
    cou_ids = ""
    for j in items:
        courses += str(j.course.course_name)+"\n"
        cou_ids += str(j.course.id)+"," 
        amt += float(j.course.sale_price)
        inv += str(j.id)
        cart_ids +=str(j.id)+","

    paypal_dict = {
        'business': settings.PAYPAL_RECEIVER_EMAIL,
        'amount': str(amt),
        'item_name': courses,
        'invoice':inv,
        'notify_url': 'http://{}{}'.format("mannu.pythonanywhere.com",
                                           reverse('paypal-ipn')),
        'return_url': 'http://{}{}'.format("mannu.pythonanywhere.com",
                                           reverse('payment_done')),
        'cancel_return': 'http://{}{}'.format("mannu.pythonanywhere.com",
                                              reverse('payment_cancelled')),

    }
    usr =User.objects.get(username=abc.user.username)
    ord = Order(cust_id=usr,cart_ids=cart_ids,course_ids=cou_ids)
    ord.save()
    ord.invoice_id = str(ord.id)+inv
    ord.save()
    abc.session["order_id"] = ord.id

    form = PayPalPaymentsForm(initial=paypal_dict)
    return render(abc, 'process_payment.html', {'form': form})

def payment_done(abc):
    if "order_id" in abc.session:
        order_id = abc.session["order_id"] 
        ord_obj = get_object_or_404(Order,id=order_id)
        ord_obj.status=True
        ord_obj.save()
        for i in ord_obj.cart_ids.split(",")[:-1]:
            cart_object = Cart.objects.get(id=i)
            cart_object.status =True
            cart_object.save()
    return render(abc,"payment_successful.html")


def payment_cancelled(abc):
    return render(abc,"payment_failed.html")
    

def order_history(abc):
    context = {}
    ch = register_table.objects.filter(user__id=abc.user.id)
    if len(ch)>0:
        data =register_table.objects.get(user__id=abc.user.id)
        context["data"]=data
    
    all_orders = []
    orders = Order.objects.filter(cust_id__id=abc.user.id).order_by("-id")
    for order in orders:
        courses = []
        for id in order.course_ids.split(",")[:-1]:
            cour = get_object_or_404(Add_Course, id=id)
            courses.append(cour)
        ord = {
            "order_id":order.id,
            "courses":courses,
            "invoice":order.invoice_id,
            "status":order.status,
            "date":order.processed_on,
        }
        all_orders.append(ord)
    context["order_history"] = all_orders
    return render(abc,"order_history.html",context)


def my_student(abc):
    context = {}
    ch = register_table.objects.filter(user__id=abc.user.id)
    if len(ch)>0:
        data = register_table.objects.get(user__id=abc.user.id)
        context["data"] = data

    courses = Cart.objects.filter(course__instructor__id=abc.user.id, status=True)
    stu =[]
    ids = []
    context["times"] = len(courses)
    for i in courses:
        us = {
            "username":i.user.username,
            "first_name":i.user.first_name,
            "last_name":i.user.last_name,
            "email":i.user.email,
            "join":i.user.date_joined,
        }
        check = register_table.objects.filter(user__id=i.user.id)
        if len(check)>0:
            prf = get_object_or_404(register_table, user__id=i.user.id)
            us["profile_pic"] = prf.profile_pic
            us["contact"] = prf.contact_number
        ids.append(i.user.id)
        count = ids.count(i.user.id)
        

        if count<2:
            stu.append(us)
            count +=1

    context["instructors"] = stu        

    return render(abc,"my_student.html",context)

