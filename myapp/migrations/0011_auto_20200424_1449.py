# Generated by Django 3.0.5 on 2020-04-24 09:19

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('myapp', '0010_auto_20200422_2115'),
    ]

    operations = [
        migrations.AlterField(
            model_name='register_table',
            name='profile_pic',
            field=models.ImageField(null=True, upload_to='profiles/%y/%m/%d'),
        ),
        migrations.CreateModel(
            name='Add_Course',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('course_name', models.CharField(max_length=250)),
                ('course_price', models.FloatField()),
                ('sale_price', models.FloatField()),
                ('course_image', models.ImageField(upload_to='courses/%y/%m/%d')),
                ('course_details', models.TextField()),
                ('course_category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='myapp.Category')),
                ('instructor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
