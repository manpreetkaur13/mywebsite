# Generated by Django 3.0.5 on 2020-04-22 09:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('myapp', '0006_auto_20200422_1524'),
    ]

    operations = [
        migrations.AlterField(
            model_name='register_table',
            name='about',
            field=models.TextField(blank=True, null=True),
        ),
    ]
