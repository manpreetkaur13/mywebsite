from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Student(models.Model):
    c =(
        ("M","Male"),("F","Female")
    )
    name = models.CharField(max_length=250)
    email = models.EmailField(max_length=200)
    roll_no = models.IntegerField(unique=True)
    fee = models.FloatField()
    gender = models.CharField(max_length=150,choices=c)
    address = models.TextField(blank=True)
    is_registered = models.BooleanField()

    def __str__(self):
        return self.name+" "+str(self.roll_no)
    class Meta():
        verbose_name_plural="Student"

class Contact(models.Model):
    name = models.CharField(max_length=250)
    email = models.EmailField(max_length=300)
    subject = models.CharField(max_length=250)
    message = models.TextField()
    added_on = models.DateTimeField(auto_now_add = True)

    def __str__(self):
        return self.name
    class Meta():
        verbose_name_plural="Contact"

class Category(models.Model):
    cat_name = models.CharField(max_length=250)
    cover_pic = models.FileField(upload_to="media/%y/%m/%d")
    description = models.TextField()
    added_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.cat_name
    class Meta():
        verbose_name_plural="Category"

class register_table(models.Model):
    c = (
         ("M","Male"),("F","Female")
    )
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    contact_number =models.IntegerField()
    gender =models.CharField(max_length=150,default="mail",null=True)
    profile_pic = models.ImageField(upload_to = "profiles/%y/%m/%d",null=True)
    age = models.CharField(max_length=250,null=True,blank=True)
    city = models.CharField(max_length=250,null=True,blank=True)
    about =models.TextField(null=True,blank=True)
    occupation =models.CharField(max_length=250,null=True,blank=True)
    added_on = models.DateTimeField(auto_now_add=True,null=True)
    update_on =models.DateTimeField(auto_now=True,null=True)


    def __str__(self):
        return self.user.username


class Add_Course(models.Model):
    instructor = models.ForeignKey(User,on_delete=models.CASCADE)
    course_name = models.CharField(max_length=250)
    course_category = models.ForeignKey(Category,on_delete = models.CASCADE)
    course_price = models.FloatField()
    sale_price = models.CharField(max_length=200)
    course_image = models.ImageField(upload_to="courses/%y/%m/%d")
    course_details = models.TextField()
    

    def __str__(self):
        return self.course_name
     

class Cart(models.Model):
    user = models.ForeignKey(User,on_delete = models.CASCADE)
    course = models.ForeignKey(Add_Course,on_delete = models.CASCADE)
    quantity = models.IntegerField()
    status = models.BooleanField(default=False)
    added_on = models.DateTimeField(auto_now_add=True,null=True)
    update_on =models.DateTimeField(auto_now=True,null=True)
    
    def __str__(self):
       return self.user.username
    class Meta():
        verbose_name_plural="Cart"

class Order(models.Model):
    cust_id = models.ForeignKey(User,on_delete = models.CASCADE)
    cart_ids = models.CharField(max_length=250)
    course_ids = models.CharField(max_length=250)
    invoice_id = models.CharField(max_length=250)
    status = models.BooleanField(default=False)
    processed_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.cust_id.username
    class Meta():
        verbose_name_plural="Order"
